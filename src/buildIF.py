from pretreatments import *
from invertedfile import *
import os
from lib import *

dirname = "../docstoteston"
# dirname = "../latimes"

def cleartmpdir() :
    print("[cleartmpdir] Clearing the tmp directory")
    for f in os.listdir(".tmp/") :
        os.remove(".tmp/"+f)

    return 0

def buildIF() :
    cleartmpdir()
    # Get the files of the
    listoffiles = os.listdir(dirname)

    for file in listoffiles :
        print("[buildIF] Indexing file " + file)
        tokens = tokenize_file(dirname +'/'+ file)
        spimiinvert(tokens)

    print("[buildIF] Merging tmp files")
    mergeif()

    cleartmpdir()

    return 0

if __name__ == '__main__':
    buildIF()
