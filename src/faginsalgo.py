import sys
from invertedfile import readtermpl
from statistics import mean


def faginsTA_and(terms, k) :
    csize = 0
    C = []
    tau = sys.maxsize
    avgmin = sys.maxsize
    # === Initialization ===
    # read the pl of each term from the IF
    pls = {}
    maxplsize = 0
    if len(terms) == 1 :
        pl = [t for t in readtermpl(terms[0])['pl'].items()]
        return pl[0:k]
    for term in terms :
        # pl = {plsize: size, pl: {docid: score}}
        pls[term] = readtermpl(term)
        # initialize an iterator to the list of docs in the pl
        pls[term]['iterator'] = iter(pls[term]['pl'])
        maxplsize = max(maxplsize, pls[term]['plsize'])
    # === Starting tht algorithm ===
    lastseen = {}
    alreadyseen = []
    while csize != k or avgmin < tau :

        # compute on one line at a time
        for term in terms :
            # pl = {plsize: size, pl: {docid: score}, iterator: iter(pl['pl'])}
            pl = pls[term]
            # iter until we find a doc that is in all the terms' pls
            try :
                ok = False
                while not ok or docid in alreadyseen :
                    docid = next(pl['iterator'])
                    ok = True
                    for t in terms :
                        if docid not in pls[t]['pl'].keys() :
                            ok = False
                            break
            except StopIteration :
                # We arrived to the end of the one of the pls
                # no more common terms between the pls
                return C
            alreadyseen.append(docid)

            # Compute the mean of the scores for each term
            score_d = mean([pls[t]['pl'][docid] for t in terms])
            lastseen[term] = pl['pl'][docid]

            print("[faginsTA_and] Selected docid = " + str(docid) + " from the PL term = " + term)
            print("[faginsTA_and] The score = " + str(score_d))
            # add the doc to C

            if csize < k :
                avgmin = min(avgmin, score_d)
                C.append((docid, score_d))
                csize += 1
            else :
                if avgmin < score_d :
                    print("[faginsTA_and] Documents having a score lower then the new document's score" )
                    print([doc[0] for doc in C if doc[1] == avgmin])
                    oldid = [doc[0] for doc in C if doc[1] == avgmin][0]
                    C.remove((oldid, avgmin))
                    C.append((docid, score_d))
                    csize = len(C)
                    avgmin = min([d[1] for d in C])
            print("[faginsTA_and] The list of results" + str(C))
            print("[faginsTA_and] avgmin = " + str(avgmin))
        # update tau
        tau = mean(list(lastseen.values()))
        print("[faginsTA_and] Last visited docs for each term : " + str(lastseen))
        print("[faginsTA_and] All lists visited once, tau = " + str(tau))

        C = sorted(C, key = lambda val: val[1], reverse = True)
    return C

def faginsTA_or(terms, k) :
    csize = 0
    C = []
    tau = sys.maxsize
    avgmin = sys.maxsize
    # === Initialization ===
    # read the pl of each term from the IF
    pls = {}
    maxplsize = 0
    if len(terms) == 1 :
        pl = [t for t in readtermpl(terms[0])['pl'].items()]
        return pl[0:k]

    for term in terms :
        # pl = {plsize: size, pl: {docid: score}}
        pls[term] = readtermpl(term)
        # initialize an iterator to the list of docs in the pl
        pls[term]['iterator'] = iter(pls[term]['pl'])
        maxplsize = max(maxplsize, pls[term]['plsize'])
    # === Starting tht algorithm ===
    lastseen = {}
    alreadyseen = []
    while csize != k or avgmin < tau :

        # compute on one line at a time
        for term in terms :
            # pl = {plsize: size, pl: {docid: score}, iterator: iter(pl['pl'])}
            pl = pls[term]
            nomoredocs = False
            # iter until we find a doc that is in all the terms' pls
            try :
                docid = next(pl['iterator'])
                while docid in alreadyseen :
                    docid = next(pl['iterator'])
            except StopIteration :
                # We arrived to the end of the one of the pls
                # no more common terms between the pls
                nomoredocs = True
                pass

            if not nomoredocs :
                alreadyseen.append(docid)

                # Compute the mean of the scores for each term
                slist = []
                for t in terms :
                    if docid in pls[t]['pl'] :
                        slist.append(pls[t]['pl'][docid])
                score_d = sum(slist)
                lastseen[term] = pl['pl'][docid]

                print("[faginsTA_and] Selected docid = " + str(docid) + " from the PL term = " + term)
                print("[faginsTA_and] The score = " + str(score_d))
                # add the doc to C

                if csize < k :
                    avgmin = min(avgmin, score_d)
                    C.append((docid, score_d))
                    csize += 1
                else :
                    if avgmin < score_d :
                        print("[faginsTA_and] Documents having a score lower then the new document's score" )
                        print([doc[0] for doc in C if doc[1] == avgmin])
                        oldid = [doc[0] for doc in C if doc[1] == avgmin][0]
                        C.remove((oldid, avgmin))
                        C.append((docid, score_d))
                        csize = len(C)
                        avgmin = min([d[1] for d in C])
                print("[faginsTA_and] The list of results" + str(C))
                print("[faginsTA_and] avgmin = " + str(avgmin))
            else :
                lastseen[term] = 0
        # update tau
        tau = mean(list(lastseen.values()))
        print("[faginsTA_and] Last visited docs for each term : " + str(lastseen))
        print("[faginsTA_and] All lists visited once, tau = " + str(tau))
        if tau == 0 :
            break
    C = sorted(C, key = lambda val: val[1], reverse = True)
    return C
