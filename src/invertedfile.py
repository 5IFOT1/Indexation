# Implementation of the SPIMI (Single-Pass In-Memory Indexing) algorithm
import _pickle as pickle
import mmap
from collections import OrderedDict
from lib import *
from shutil import copyfile
import operator
import os

global plfilename, vocfilename

# A token = (term, nbrOccurences, docId)
def spimiinvert(blockOfTokens) :
    VOC = {}
    global nb_dict_tmp_files
    with open(vocfilename + str(nb_dict_tmp_files) + ".bin", "wb") as vocfile :
        with open(plfilename + str(nb_dict_tmp_files) + ".bin", "ab") as plfile :
            for token in blockOfTokens :
                term, docid, nbocc = token[0], token[1], token[2]

                if term not in VOC :
                    pl = addtodictionary(VOC, term)
                else :
                    pl = getpostingslist(VOC, term)

                addtopostingslist(pl, docid, nbocc)
            writeblocktodisk(VOC, vocfile, plfile)
    vocfile.close()
    plfile.close()

    nb_dict_tmp_files += 1

    return 0

def addtodictionary(dictionary, vterm) :
    dictionary[vterm] = {'plsize': 0, 'pl': {}}
    return dictionary[vterm]

def getpostingslist(dictionary, vterm) :
    return dictionary[vterm]

def addtopostingslist(pl, docid, score) :
    pl['pl'][docid] = score
    pl['plsize'] += 1

    return pl

def writeblocktodisk(dictionary, vocfile, plfile) :
    # map the plfile to memory
    offsetplfile = plfile.tell()
    for term in dictionary :
        # write the pickled pl to file
        pl = dictionary[term]
        pickle.dump(pl, plfile)
        dictionary[term] = offsetplfile
        offsetplfile = plfile.tell()

    # Write the whole pickled dictionary to file
    pickle.dump(dictionary, vocfile)
    return 0

# Read a tuple (plsize, pl)
def readtermpl(term) :
    global nb_dict_tmp_files, ifvocfilename, ifplfilename
    with open(ifvocfilename, "rb") as vocfile :
        # load the dictionary
        voc = pickle.load(vocfile)
        if term in voc :
            # get the offset of the term's pl in the plfile
            ploffset = voc[term]
            with open(ifplfilename, "rb") as plfile :
                plfile.seek(ploffset)
                pl = pickle.load(plfile)
            plfile.close()
        else :
            pl = {'plsize': 0, 'pl': {}}
    vocfile.close()
    return pl

# Merge inverted files
def mergeif() :

    global nb_dict_tmp_files
    global ifvocfilename, ifplfilename
    # load all the tmp dictionaries from disk to memory
    if nb_dict_tmp_files < 2 :
        # TODO  sort the pls
        # create the final IF
        copyfile(vocfilename +"0.bin", ifvocfilename)
        copyfile(plfilename +"0.bin", ifplfilename)
        return 0

    if nb_dict_tmp_files == 2 :
        merge2ifsortpls(vocfilename + "0.bin",
        vocfilename + "1.bin",
        plfilename + "0.bin",
        plfilename + "1.bin",
        vocfilename + "1merged.bin",
        plfilename + "1merged.bin")
    else :
        merge2if(vocfilename + "0.bin",
        vocfilename + "1.bin",
        plfilename + "0.bin",
        plfilename + "1.bin",
        vocfilename + "1merged.bin",
        plfilename + "1merged.bin")


        for i in range(2,nb_dict_tmp_files-1) :
            # merge the files 2 by 2
            merge2if(vocfilename + str(i-1) + "merged.bin",
            vocfilename + str(i) + ".bin",
            plfilename + str(i-1) + "merged.bin",
            plfilename + str(i) + ".bin",
            vocfilename + str(i) + "merged.bin",
            plfilename + str(i) + "merged.bin")
            # remove the merged files
            os.remove(vocfilename + str(i-1) + "merged.bin")
            os.remove(vocfilename + str(i) + ".bin")
            os.remove(plfilename + str(i-1) + "merged.bin")
            os.remove(plfilename + str(i) + ".bin")

        merge2ifsortpls(vocfilename + str(nb_dict_tmp_files-2) + "merged.bin",
        vocfilename + str(nb_dict_tmp_files-1) + ".bin",
        plfilename + str(nb_dict_tmp_files-2) + "merged.bin",
        plfilename + str(nb_dict_tmp_files-1) + ".bin",
        vocfilename + str(nb_dict_tmp_files-1) + "merged.bin",
        plfilename + str(nb_dict_tmp_files-1) + "merged.bin")

    # create the final IF
    copyfile(vocfilename + str(nb_dict_tmp_files-1) + "merged.bin", ifvocfilename)
    copyfile(plfilename + str(nb_dict_tmp_files-1) + "merged.bin", ifplfilename)

    return 0

def merge2if(if1name, if2name, plf1name, plf2name, ifmname, plmname) :
    with open(if1name, "rb") as if1, open(if2name, "rb") as if2, open(plf1name, "rb") as plf1, open(plf2name, "rb") as plf2, open(ifmname, "wb") as mergedif, open(plmname, "ab") as mergedpl :
        # load the 2 dictionaries to merge
        voc1 = pickle.load(if1)
        voc2 = pickle.load(if2)

        for term, offset2 in voc2.items() :
            plf2.seek(offset2)
            pl2 = pickle.load(plf2)
            if term in voc1 :
                # Append pl2 to pl1 & sum the sizes
                offset1 = voc1[term]

                plf1.seek(offset1)
                pl1 = pickle.load(plf1)

                pl1['pl'].update(pl2['pl'])
                pl = {'plsize': pl1['plsize'] + pl2['plsize'], 'pl': pl1['pl']}

            else :
                pl = pl2

            voc1[term] = mergedpl.tell()
            pickle.dump(pl, mergedpl)

        # Update the pl offsets of the terms that are in voc1 but not in voc2
        for term, offset1 in voc1.items() :
            if term not in voc2 :
                plf1.seek(offset1)
                pl = pickle.load(plf1)
                # update offset in voc1
                voc1[term] = mergedpl.tell()
                # write the pl to disk
                pickle.dump(pl, mergedpl)
        # Write the merged vocabulary to disk
        pickle.dump(voc1, mergedif)
    if2.close()
    if1.close()
    plf1.close()
    plf2.close()
    mergedif.close()
    mergedpl.close()
    return 0

def merge2ifsortpls(if1name, if2name, plf1name, plf2name, ifmname, plmname) :
    with open(if1name, "rb") as if1, open(if2name, "rb") as if2, open(plf1name, "rb") as plf1, open(plf2name, "rb") as plf2, open(ifmname, "wb") as mergedif, open(plmname, "ab") as mergedpl :
        # load the 2 dictionaries to merge
        voc1 = pickle.load(if1)
        voc2 = pickle.load(if2)

        for term, offset2 in voc2.items() :
            plf2.seek(offset2)
            pl2 = pickle.load(plf2)
            if term in voc1 :
                # Append pl2 to pl1 & sum the sizes
                offset1 = voc1[term]

                plf1.seek(offset1)
                pl1 = pickle.load(plf1)

                pl1['pl'].update(pl2['pl'])
                pl = {'plsize': pl1['plsize'] + pl2['plsize'], 'pl': pl1['pl']}

            else :
                pl = pl2

            voc1[term] = mergedpl.tell()
            # sort the pl
            pl['pl'] = OrderedDict(sorted(pl['pl'].items(), key=lambda doc: doc[1], reverse = True))
            pickle.dump(pl, mergedpl)

        # Update the pl offsets of the terms that are in voc1 but not in voc2
        for term, offset1 in voc1.items() :
            if term not in voc2 :
                plf1.seek(offset1)
                pl = pickle.load(plf1)
                # update offset in voc1
                voc1[term] = mergedpl.tell()
                # sort the pl
                pl['pl'] = OrderedDict(sorted(pl['pl'].items(), key=lambda doc: doc[1], reverse = True))
                # write the pl to disk
                pickle.dump(pl, mergedpl)
        # Write the merged vocabulary to disk
        pickle.dump(voc1, mergedif)
    if2.close()
    if1.close()
    plf1.close()
    plf2.close()
    mergedif.close()
    mergedpl.close()
    return 0

def orderpls(srcfile, destfile) :
    with open(srcfile, "rb") as nonordered, open(destfile, "wb") as ordered :
        print("[orderpls] Sort the postings lists in the reverse order of the documents' scores ...")
        nonordered.seek(0)
        while True :
            try :
                pl = pickle.load(nonordered)
                pl['pl'] = sorted( pl['pl'].items(), key = operator.itemgetter(1), reverse = True)
                pickle.dump(pl, ordered)
            except EOFError :
                print("[orderpls] PLs sorted")
                break
    nonordered.close()
    ordered.close()
