# External imports
from pyparsing import *
from bs4 import BeautifulSoup
import nltk
from nltk import *
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk.corpus import stopwords
import string


#Directory containing test files
dirname = "../docstoteston"

# NLTK's default German stopwords
default_stopwords = set(nltk.corpus.stopwords.words('english'))

# Method used for tokenising both documents and queries
def tokenize_tokens(textwithtags):

    # Remove the tags form the text
    soup = BeautifulSoup(textwithtags, "html.parser")
    tagfreetext = soup.get_text()
    list_tokens = word_tokenize(tagfreetext)
    # Get a list of words in lowercase whith the frequencies associated to each word
    # Retain only alphanumeric tokens
    tokens = [ token.lower() for token in list_tokens if token.isalnum()]

    # Stemming words seems to make matters worse, disabled
    stemmer = nltk.stem.snowball.EnglishStemmer()
    tokens = [stemmer.stem(token) for token in tokens]

    # Remove stopwords
    tokens = [token for token in tokens if token not in default_stopwords]

    # list_tokens[1] is the document id
    if len(list_tokens) > 1:
        return tokens,list_tokens[1]

    # Query case
    else:
        return tokens,0

# Calculate each token frequence
def tokenize (textwithtags) :

    tokens, docid = tokenize_tokens(textwithtags)
    fdist = FreqDist(tokens)
    terms = [(token,round(fdist.freq(token),8)) for token in list(set(tokens))]

    return terms, docid

def tokenize_file(file) :

    list_documents=[]

    # Define the first and the last tag
    anchorStart,anchorEnd = makeHTMLTags("DOC")

    #Get the body selection
    anchor = anchorStart + SkipTo(anchorEnd).setResultsName("document") + anchorEnd

    file = open(dirname+'/'+file);
    file_content = file.read()

    #Each tokens is a document
    for tokens,start,end in anchor.scanString(file_content):

        # Get the list of words contained in the document
        listoftokens, docId = tokenize(tokens.document)
        list_documents.extend((term, int(docId), nbOcc) for (term, nbOcc) in listoftokens)

    file.close()

    return list_documents
