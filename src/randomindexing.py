from lib import *
import _pickle as pickle
import random
import numpy as np

global ifplfilename, ifvocfilename, randomdegree, dimensionality

def buildrandomindex() :

    with open(ifvocfilename, "rb") as vocfile, open(ifplfilename, "rb") as plfile :

        # Get the vocabulary from the VocFile
        voc = pickle.load(vocfile)

        indexvectors = {}
        contextvectors = {}
        # Constract a context vector to each term in the vocabulary
        for item in voc.items() :
            term = item[0]
            # load the pl from the plfile
            plfile.seek(item[1])
            plobj = pickle.load(plfile)
            pl = plobj['pl']

            # Initialize the term's context vector with zeros
            contextvector = np.zeros(dimensionality, dtype=np.int8)
            indexvectors[term] = contextvector

            for docid in pl :
                if docid in indexvectors :
                    indexvector = indexvectors[docid]
                else :
                    indexvector = random_vector(docid, dimensionality, randomdegree)
                    indexvectors[docid] = indexvector
                # add the index vector to the context vector (the term's context vector)
                contextvector += indexvector
            contextvectors[term] = contextvector
        # Write the random index to file
        global randomindexfile
        with open(randomindexfile, "wb") as randomindexfile :
            pickle.dump(contextvectors, randomindexfile)
        randomindexfile.close()

    vocfile.close()
    plfile.close()

# Find the n nearest neighbours
def nearestneighbours(term, n, randomindex) :

    similarities = {}
    # Compute the cosinus similarity between the term and all the other terms of the vocabulary
    if term not in randomindex :
        return []
    termcontext = randomindex[term]
    for t, context in randomindex.items() :
        # TODO similarity computation gives nan sometimes ???
        similarities[t] = cosine(context, termcontext)

    # sort the similarity victor to get the nearest n neighbours
    neighbours = sorted(similarities.items(), key=lambda item: item[1], reverse = True)[1:n+1]

    return neighbours

def neighbours(terms, n) :
    global randomindexfile
    with open(randomindexfile, "rb") as randomindexfile :
        randomindex = pickle.load(randomindexfile)
        neighbours = []
        for t in terms :
            neighbours.extend(nearestneighbours(t, n, randomindex))
    randomindexfile.close()
    return neighbours

# https://github.com/ekgren/Random_Indexing/blob/master/Random_Indexing/RandomIndexing.py
def random_vector(random_seed, dimensionality, random_degree):
    """
    Creates and return a numpy int8 Random Index vector.
    :param random_seed:
    :param dimensionality:
    :param random_degree:
    :rtype ndarray:
    """
    random.seed(random_seed)

    vector = np.zeros(dimensionality, dtype=np.int8)

    for i in range(random_degree):
        if i % 2 == 0:
            vector[random.randint(0, dimensionality - 1)] = -1
        else:
            vector[random.randint(0, dimensionality - 1)] = 1

    return vector

# https://github.com/ekgren/Random_Indexing/blob/master/Random_Indexing/RandomIndexing.py
def cosine(u, v):
    """
    """

    u_norm = np.linalg.norm(u)
    v_norm = np.linalg.norm(v)

    if (u_norm > 0.0) and (v_norm > 0.0):
        dist = np.dot(u, v) / (u_norm * v_norm)
    else:
        dist = 0.0

    return dist


if __name__ == '__main__':
    buildrandomindex()
