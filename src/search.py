from pretreatments import *
from faginsalgo import *
import sys

def main(operator, query, k = 5) :
    terms = tokenize_tokens(query)[0]
    results = []
    if operator == "or" :
        results = faginsTA_or(terms, k)
    else :
        if operator == "and" :
            results = faginsTA_and(terms, k)
        else :
            print("[Search] Wrong Operator, only supports 'and' or 'or' operators.")
            return

    print("==> Search Results")
    print(results)

if __name__ == '__main__':
    try:
        k = int(sys.argv[3])
        main(sys.argv[1], sys.argv[2], k )
    except :
        print("[Search] You did not specify the number of results. Default = 5")
        main(sys.argv[1], sys.argv[2])
        pass
