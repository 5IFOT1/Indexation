#Internal imports
from pretreatments import *

# External imports
from pyparsing import *

#Directory containing test files
dirname = "../docstoteston"

#Each document is defined by a dictionary composed by its parent file id, its id and its body
list_documents=[]


def getDocs(file_to_read_id):

    # Define the first and the last tag
    anchorStart,anchorEnd = makeHTMLTags("DOC")

    #Get the body selection
    anchor = anchorStart + SkipTo(anchorEnd).setResultsName("document") + anchorEnd

    #File to read
    file_content = open(dirname+'/'+file_to_read_id).read()

    #Each tokens is a document
    for tokens,start,end in anchor.scanString(file_content):

        # Get the list of words contained in the document
        listofwords = removetagstokenize(tokens.document)

        # document_id is always the first token
        list_documents.append(dict(parent_id=file_to_read_id, id=int(listofwords[0]), body=listofwords))

    return list_documents
