from pretreatments import *
from querytexts import ranked_queries_or, ranked_queries_and
import sys

def main(operator, query, k = 5) :
    results = []
    # Get tokens from query input
    tokens,docid = tokenize_tokens(query)

    if operator == "or" :
        results = ranked_queries_or(tokens, k)
    else :
        if operator == "and" :
            results = ranked_queries_and(tokens, docid, k)
        else :
            print("[NaiveSearch] Wrong Operator, only supports 'and' or 'or' operators.")
            return

    print("==> Naive Search Results")
    print(results)

if __name__ == '__main__':
    try:
        k = int(sys.argv[3])
        main(sys.argv[1], sys.argv[2], k )
    except :
        print("[Naive Search] You did not specify the number of results. Default = 5")
        main(sys.argv[1], sys.argv[2])
        pass
