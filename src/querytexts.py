# External imports
import re
import numpy as np
from lib import *
import nltk
from operator import itemgetter
from sys import *
from sortedcontainers import SortedDict
from statistics import mean

# Internal imports
from invertedfile import *
from pretreatments import *

global ifvocfilename

# Ranked queries, native algorithm OR
def ranked_queries_or(terms, k):

    # result = {docId:score,....}
    result = {}

    for term in terms:

        # read the pl of each term from the IF
        pl_array = readtermpl(term)

        # At the beginning, result is equal to the first term's pl
        if not result:
            result = pl_array['pl']
        else:
            for docid in pl_array['pl'].keys():
                # If the document already exits in the result list, its score will be updated
                if docid in result.keys():
                    result[docid] = round(sum([pl_array['pl'][docid],result[docid]]),8)

                # Else, new entry is created
                else:
                    result[docid] = pl_array['pl'][docid]

    # Print top k documents
    res = sorted(result.items(), key=lambda item: item[1], reverse = True)[:k]
    return res

# Ranked queries, native algorithm AND
def ranked_queries_and(tokens, docid, k):

    # read the pl of each term from the IF
    pls = {}
    minplsize = sys.maxsize
    for term in tokens :
        # pl = {plsize: size, pl: {docid: score}}
        pls[term] = readtermpl(term)
        pls[term]['pl'] = SortedDict(pls[term]['pl'])
        if minplsize > min(minplsize, pls[term]['plsize']):
            minplsize = min(minplsize, pls[term]['plsize'])
            # The term with the smallest pl
            term_min = term
            # The smallest pl
            pl_array_smallest = pls[term]['pl']

    # Delete the smallest pl from pls array
    pls = remove_pl(pls, term_min)
    # Remove the token with the smallest pl
    tokens = [token for token in tokens if token != term_min]

    # At the beginning, result list is equal to the smallest pl
    result = pl_array_smallest

    for docid in pl_array_smallest.keys():

        scores_list = []
        scores_list.append(pl_array_smallest[docid])
        doc_is_found = True

        for term in tokens:
            # If the document already exists in other term pl, its score will be added to scores_list
            if docid in pls[term]['pl'].keys():
               scores_list.append(pls[term]['pl'][docid])
            else :
                doc_is_found = False
                break

        # Update document score
        if doc_is_found == True:
            result[docid] = mean(scores_list)

        else:
            # If there is more than one word and the document doesn't exists in an another pl, he will be removed from the result list
            if(len(tokens) > 0) :
                result = remove_pl(result,docid)

    res = sorted(result.items(), key=lambda item: item[1], reverse = True)[:k]
    return res


# Remove an element from a dictionary by its key
def remove_pl(dictionary, element):
    dictionary_temp = dict(dictionary)
    del dictionary_temp[element]
    return dictionary_temp
