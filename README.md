# Algorithms and data structures for indexing large volumes of textual data

## Environment initialization
Start by clonnig the project using :  
```bash
    $ git clone https://gitlab.com/5IFOT1/Indexation.git
```  
You get a directory named `Indexation`, get into that directory :  
```bash
    $ cd Indexation
```  
**NB :** You are on `develop`, think of creating a new branch to start working on a new feature :p :  
```bash
    $ git checkout -b feature/myfeaturename
```  
### Setup and/or start a python virtual environemnt
Since python packages exist in numerous versions, and to avoid dealing with different versions installed conflicts, we're going to use a python
[virtual environement](https://docs.python.org/3/tutorial/venv.html)  
#### Installation
Use the command below to install `python3-venv` :  
```bash
    $ sudo apt-get install python3-venv
```  
#### Create and/or activate a virtual environment
Create a virtual environment named `virtualenv` (ignored by the git, if you choose another name, please add it to the `.gitignore` file !)   
```bash
    $ python3 -m venv virtualenv
```  
**NB :** You only need to create your virtual environment once !  
Activate the virtual environment **each time you want to run the project** :
```bash
    $ source virtualenv/bin/activate
```  
To install the required python packages needed to run the project, or new added ones use the command below once your virtual environment is activated :  
```bash
    $ pip install -r requirements.txt
```  
#### Install new packages
To install a new package use `pip install pckg_name` (once your virtual environement is activated!)  
Don't forget to add the new package to `requirements.txt` using the command in the **project root** :    
```bash
    $ pip freeze > requirements.txt
```  
### Requirements
* Installing python3 and python3-venv
* Creating and activating a virtual environement
* Installing the required packages contained in `requirements.txt` in the virtual environement
* You need to create an empty folder `.txt/` under `src/` needed by the module building the inverted file, since it is not done automatoically.  

**Now you're good to go farther :)**
## The project structure
```
    \
    |
    |----- docstoteston\
    |
    |----- latimes\  
    |
    |----- src\
```  
`doctimes` a subset of the dataset we use to test our algorithms, since processing all the documents contained in the whole dataset `latimes` takes a little bit too much time.  
Let's now give some details on our code organization :
  
### Pretreatment
```
   src\
    |
    |----- pretreatments.py
  
```
For each file in `doctimes` or`latimes`, the method `tokenize_file()` is called on. First, its documents are extracted using `pyparsing` library. Then, each document is tokenised using the method `tokenize_tokens()`. For this task, we used the `nltk` library. The tokenisation process is started by removing tags using `BeautifulSoup`. Then, the document is tokenised. Only alphanumeric tokens are retained. After stemming tokens, stop words are removed. 

The method `tokenize_file()` returns a list of tuples as described below: 
``` python
[(term, docId, termfrequency),....]

```

### Building the inverted file (IF)
```
   src\
    |
    |----- invertedfile.py
    |
    |----- buildIF.py

```  
To build the inverted file, after some reading, we decided to implement the `SPIMI` (Single-Pass In Memory-Indexing) algorithm, building intermediate small IFs written each time to disk. After all documents have been indexed, we merge the IFs, **two by two** to get a final Inverted index used by the search algorithms.  
Our Inverted Index consists on **two files** :  

* PLfile.bin
* VOCfile.bin  

The `VOCfile.bin` contains a serialized python dictionary (using `pickle`) mapping **terms** as keys to offsets of their corresponding **Posting lists** in the `PLfile.bin` :  
```json
    {
        ...
        "term": offsetInPLfile,
        ...
    }
```  
The `PLfile.bin` contains sequentially stored **posting lists (PLs)**, each PL is a serialized dictionary as described bellow :  
```json
    {
        "plsize" : size,
        "pl" : {
            docid: score
        }
    }
```  
We chose to represent ou posting list by a **docid => score** dictionary to make the direct access on docids more efficient, since the last merge of PLs when building the IF sorts the PLs on the inverse order of their scores.  

While the `SPIMI` and `merge` algorithms are implemented in the `invertedfile` module, the build using these algorithms is in the `buildIF` module and can be started using the command :  
```bash
    $ python buildIF.py
```  

### Search 
```
   src/
    |
    |----- querytexts.py
    |
    |----- faginsalgo.py
    |
    |----- search.py
    |
    |----- naivesearch.py
```  
We've implemented two search algorithms (**OR** and **AND** Operators): 
* A naive one implemented in `querytexts.py`, that works on the shortest pl to answer **And** queries and passes throw all the items of all the pls to answer **OR** queries.  
* Fagin's Threshold algorithm implemented in `faginsalgo.py`.

The `search` and `naivesearch` modules use the respectively `Fagin's TA` and `naive algorithms` to answer queries. They both take **three parameters**:  
* **Logical operator** : has to be a string taking one of the two values {"and", "or"}
* **Search query** : a string (stop words are not considered!)
* **Number of answers** : is optional, if not provided a defaut value of 5 is used

Here is an example :
```bash
    $ python search.py or "gaza israel" 8
```
The search algorithm gives a list of the top k documents sorted by their scores. The score is computed using an aggregation function (**sum** to OR queries, **mean** to AND queries).  
List of top 8 documents (docid, freq) :
```bash
    ==> Search Results
    [(156504, 0.09708738), (1601, 0.0875), (156501, 0.05921053), (101, 0.04245283), (1004, 0.04190751), (931, 0.04102564), (473, 0.03608248), (156502, 0.0321489)]

```   

### Terms similarity 
```
   src\
    |
    |----- randomindexing.py
    |
    |----- neighbours.py
    |
    |----- extendsearch.py
    |
    |----- extendnaivesearch.py
```  
In order to give more pertinent results, we implemented the random indexing algorithm (`randomindexing.py`) to get the best k nearest neighbours of a given term.  
We first built an index we write to disk (`randomindexfile.bin`), using the inverted file, that is a dictionary mapping **terms** to **context vectors** of a predefined size (dimentionality = 100 in our case) :  
```json
    {
        ...
        "term" : [...,0,1,0,0,2,40,-4,...],
        ...
    }
```  
To build the random index run the following command :  
```bash
    $ python randomindexing.py
```  
Random indexing is a solution to reduce the problem of dimentionality (curse of dimentionality!) both when building the context matrix and serching the k nearest neighbours of a term.  
To get the nearest neighbours, we compute the cosinus similarity between the context vector of the given term, and all other terms' context vectors.  
Run the following command to get the 5 nearest neighbours of a given term :  
```bash
    $ python neighbours.py term
```  
Example (dimentionality of vectors = **100**, number of 1 and -1 in an index vector (**pair**) = **10**):  
```bash
    $ python neighbours.py gaza
```  
Gives quite good logical results (on an index of a 10 files dataset):  
```bash
    [('yitzhak', 0.72482627872363881), ('jerusalem', 0.68296946571829797), ('palestinian', 0.66116287867485191), ('palestin', 0.63209889851384493), ('likud', 0.62684858650381992)]
```  
Finally, we plugged the algorithm getting the k nearest neighbours to the search functions (extendsearch.py and extendnaivesearch.py) to add **the most similar** term to each given term in the query to the search. The results are different.  
The query in the following example has already been answered using the `search` module, here are the results using the `extendsearch` module :  
```bash
    $ python extendsearch.py or "gaza israel" 8
    ==> Search Results
    [(156504, 0.09708738), (1601, 0.0875), (156501, 0.05921053), (931, 0.051282060000000004), (156502, 0.04568528), (1004, 0.04479769000000001), (101, 0.04245283), (473, 0.038659799999999994)]

```
## Results
We've made some measures to compare the algorithms according to time of execution, the size of the dataset we worked on, ... They are available on the Presentation we provide with the project.
